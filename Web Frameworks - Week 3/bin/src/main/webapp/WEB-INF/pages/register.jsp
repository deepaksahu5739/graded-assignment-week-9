<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>


<link rel="stylesheet" type="text/css" href="style.css">


</head>
<body>

	<header>
		<a href="a" class="logo"><i>Ebooks</i></a>

		<nav class="navbar">
			<a href="index">Home</a>
			 <a href='login'>Login Here</a>
				


		</nav>
	</header>

	<section class="home" id="home">
	
	 <div class="main1">
        <div class="register">
            <h2>Register Here</h2>
            <form id="register"  action="register" method="post">
                <label>First Name : </label>
                <br>
                <input type="text" name="fname"
                id="textbox" placeholder="First Name">
                <br><br>

                <label>Last Name : </label>
                <br>
                <input type="text" name="lname"
                id="textbox" placeholder="Last Name">

                <br><br>

                <label>Phone No : </label>
                <br>
                <input type="text" name="phone" id="textbox">
                <br><br>

                <label>Email : </label>
                <br>
                <input type="email" name="email" id="textbox" placeholder="Enter Email">
                <br> <br>
                <label>Password : </label>
                <br>

                <input type="password" name="password" id="textbox" placeholder="Enter Password">
                <br> <br> 

                <input type="submit" value="Register" name="register"
                id="register1" name="register">



            </form>
        </div>
    </div>
		
	</section>

</body>
<style>
@import url('https://fonts.googleapis.com/css2?family=Nunito:ital,wght@0,200;0,300;0,400;0,700;1,600&display=swap');

:root{
    --red:#ff3838;
}

*{
    font-family: 'Nunito', sans-serif;
    margin: 0; padding: 0;
    box-sizing: border-box;
    outline: none; border: none;
    text-decoration: none;
    text-transform:capitalize;
    transition: all .2s linear;
    
}

*::selection{
    background:var(--red);
    color: #fff;
}

html{
    font-size: 62.5%;
    overflow-x: hidden;
    scroll-behavior: smooth;
    scroll-padding-top: 6rem;
}

body{
    background: #f7f7f7;
    
}

section{
    padding: 2rem  7rem 1%;
    

}
section .movielist{
    padding: 0;
    margin: 0;
}

header{
    position: fixed;
    top:0; left: 0; right: 0;
    z-index: 1000;
    display: flex;
    align-items: center;
    justify-content: space-between;
    background: #fff;
    padding: 2rem 9%;
    box-shadow: 0 .5rem 1rem rgba(0,0,0,.1);

}

header .logo{
    font-size: 2.5rem;
    font-weight: bolder;
    color: #666;
}

header .logo i{
    padding-right: .5rem;
    color: var(--red);
}

header .navbar a{
    font-size: 2rem; 
    margin-left: 2rem;
    color: var(--red);
}
header .navbar button{
    font-size: 2rem; 
    margin-left: 2rem;
    color: #666;
    background-color: #fff;

}

header .navbar a:hover{
    color: #666;
}

header ,.navbar button:hover{
    color: var(--red);
}

.home{
    display: flex;
    flex-wrap: wrap;
    gap: 1.5rem;
    min-height: 100vh;
    align-items: center;
    
    background-size: cover;
    background-position: center;
    
}

.home .content{
    flex: 1 1 40rem;
    padding-top: 6.5rem;
}

.home .image{
    flex: 0 1 52rem;
}

.home .image img{
    width: 100%;
    padding: 1rem;
    animation: float 3s linear infinite;
}

.home .content h3{
    font-size: 5rem;
    color:var(--red);
    transition: var(--red) 2s linear;

}

.home .content p{
    font-size: 1.7rem;
    color: #333;
    padding: 1rem 0;

}

.fav{
    display: inline-block;
    padding: .8rem 3rem;
    border: .2rem solid var(--red);
    color: var(--red);
    cursor: pointer;
    font-size: 1.7rem;
    border-radius: .5rem;
    position: relative;
    overflow: hidden;
    z-index: 0;
    margin-top: 1rem;
    margin-right: 1rem;

}

.fav::before{
    content: '';
    position: absolute;
    top: 0; right: 0;
    width: 0%;
    height: 100%;
    background: var(--red);
    
    z-index: -1;
}

.fav:hover::before{
    width: 100%;
    left: 0;
}
.fav:hover{
    color: #fff;
    background: var(--red);
    

}



@media(max-width:991px){
    html{
        font-size: 55%;
    }

    header{
        padding: 2rem;
    }
    section{
        padding: 2rem;
    }
}

@media(max-width:768px){
    header .navbar{
        position: absolute;
        top:100%; left: 0; right: 0;
        background: #f7f7f7;
        border-top: .1rem solid rgba(0,0,0,.1);
        clip-path: polygon(0 0, 100% 0, 100% 0, 0 0);
    }

    header .navbar a{
        margin: 1.5rem;
        padding: 1.5rem;
        display: block;
        border: 2rem solid rgba(0,0,0,.1);
        border-left: .5rem solid var(--red);
        background: #fff;
    }
    header .navbar button{
        margin: 1.5rem;
        padding: 1.5rem;
        display: block;
        border: 2rem solid rgba(0,0,0,.1);
        border-left: .5rem solid var(--red);
        background: #fff;
        background-color: #fff;

    }
}

@media(max-width:991px){
    html{
        font-size: 50%;
    }
}
     
  
    


div.main1{
    width: 400px;
    margin: 100px auto 0px auto;
}

h2{
    text-align:center;
    padding: 20px;
    font-family: sans-serif;

}

div.register{
    background-color: rgba(0,0,0,0.5);
    width: 100%;
    font-size: 18px;
    border-radius: 10px;
    border: 1px soid rgba(255, 255,255,0.3);
    box-shadow: 2px 2px 15px rgba(0,0,0,0.3);
    color: #fff;
    
}

form#register{
    margin: 40px;
}
label{
    font-family: sans-serif;
    font-size: 18px;
    
}

input#textbox{
    width: 300px;
    border: 1px solid #ddd;
    border-radius: 3px;
    outline: 0;
    padding: 7px;

    background-color: #fff;
    box-shadow: inset1px 1px 5px rgba(0,0,0,0.3);


}

input#register1{
    width: 200px;
    padding: 7px;
    font-size: 16px;
    font-family: sans-serif;
    font-weight: 600;
    border-radius: 3px;
    background-color: rgba(250, 100,0,0.8);
    color: #fff;
    cursor: pointer;
    border: 1px solid rgba(255, 255,255,0.3);
    box-shadow: 1px 1px 5px rgba(0,0,0,0.3);
    margin-bottom: 30px;
}

 .box{
        flex: 1 1 30rem;
        position: relative;
        overflow: hidden;
        box-shadow: 0 .5rem 1rem rgba(0,0,0,.1);
        
        cursor: pointer;
        border-radius: .5rem;
        padding-top: -7px;

    }
   .box .content{
        text-align: center;
        background: #fff;
        padding: 2rem;
    }
   .box .content img{
        margin: .5rem 0;

    }
  .box .content h3{
        font-size: 2.5rem;
        color: #333;
    }
 .box .content p{
        font-size: 1.5rem;
        color: #666;
        padding: 1rem 0;
    }

 r .box .content para:hover{

        background: transparent;
        display: var(--red);
        text-align: center;
        font-size: 1.5rem;

    }




</style>
</html>