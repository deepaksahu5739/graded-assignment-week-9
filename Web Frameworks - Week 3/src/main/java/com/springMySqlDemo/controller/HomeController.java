package com.springMySqlDemo.controller;
import com.springMySqlDemo.database.FavBookDatabase;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.springMySqlDemo.database.BookDatabase;
import com.springMySqlDemo.database.FavBookDatabase;
import com.springMySqlDemo.database.UserDatabase;
import com.springMySqlDemo.model.Book;
import com.springMySqlDemo.model.FavBooks;
import com.springMySqlDemo.model.Users;
import com.springMySqlDemo.repositry.UserRepositry;

@Controller
public class HomeController {
	
	public HomeController() {
		System.out.println("home called");
	}
	
	@Autowired
	private UserDatabase db;
	
	@Autowired
	private BookDatabase bd;
	
	@Autowired
	private FavBookDatabase fd;
		
	
	@RequestMapping
	public String indexPage() {
		
		System.out.println("index page called");
		return "index";
	}
	
	@GetMapping("/login")
	public String loginPage() {
		System.out.println("login called");
		return "login";
	}
	
	@GetMapping("/register")
	public String registerPage() {
		System.out.println("register page called");
		return "register";
	}
	
	@PostMapping("/login")
	public String getdata(@ModelAttribute("email") String email, String password, Map<String, String> map, HttpSession session) throws Exception {
		System.out.println(email);
		System.out.println(password);
		Users user = db.getUserByEmail(email);
		System.out.println(user);
		if(user.getEmail().equals(email) && user.getPassword().equals(password)) {
			session.setAttribute("email", user.getFirstName()+" "+ user.getLastName());
			session.setAttribute("password", password);			
			return "viewBooks";
			
		}
		return "login";
		
	}
	
	
	@PostMapping("/register")
	public String getdataRegister(@ModelAttribute("email") String email, String fname, String lname, String phone, String password) throws Exception  {
		System.out.println(email);
		System.out.println(fname);
		System.out.println(lname);
		System.out.println(phone);
		System.out.println(password);
		Users user = new Users();
		user.setFirstName(fname);
		user.setLastName(lname);
		user.setPhone(phone);
		user.setEmail(email);
		user.setPassword(password);
		if(db.insertUser(user)) {
			System.out.println("user insetred");
			return "login";
			
		}
		
		System.out.println("insertion failed");
		return "register";
	}
	
	
	@GetMapping("/viewBooks")
	public String getBooks(HttpSession session) {
		System.out.println("book called");
		List<Book> book1 = bd.getBookData();
		
		for(Book b: book1) {
			System.out.println(b);
		}
		session.setAttribute("bookList", book1);
		return "viewBooks";
		
	}

	@GetMapping("/viewAsGuest")
	public String getBookForGuest(HttpSession session) {
		System.out.println("get book called");
		
		List<Book> book1 = bd.getBookData();

		for(Book b: book1) {
			System.out.println(b);
		}
		session.setAttribute("bookList", book1);
		return "viewAsGuest";
		
		
		
	}
	
	@PostMapping("/welcome")
	public String favPage(@ModelAttribute("inputName") String inputName, String bookPoster, String bookAuthor, String bookyear,
			HttpSession session) throws Exception {
		System.out.println("fav page called");
		System.out.println(inputName);
		System.out.println(bookPoster);
		System.out.println(bookAuthor);
		System.out.println(bookyear);
		FavBooks fbook = new FavBooks();
		fbook.setBookName(inputName);
		fbook.setActhor(bookAuthor);
		fbook.setPoster(bookPoster);
		fbook.setPublishYear(bookyear);
		if(fd.insertFavBook(fbook)) {
			System.out.println("fav added into list");
		}else {
			System.out.println("fav not added");
		}
		
		
		session.setAttribute("movieName", fbook);
		return "welcome";
	}
	
	@GetMapping("/favbooks")
	public String favbookspage(HttpSession session) {
		System.out.println("fav books list page called");
		
		List<FavBooks> book2 = fd.getFavBooks();
		for(FavBooks b: book2) {
			System.out.println(b);
		}
		session.setAttribute("favbook", book2);
		return "favbooks";
	}
	
	@GetMapping("/index")
	public String goHomePage() {
		return "index";
	}
	
	@GetMapping("/logout")
	public String logoutPage() {
		
		return "login";
	}
	
	
	
	
	
	
	
	
	

}
