package com.springMySqlDemo.database;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springMySqlDemo.model.Book;
import com.springMySqlDemo.model.Users;
import com.springMySqlDemo.repositry.UserRepositry;


@Service
public class UserDatabase {

	@Autowired
	private UserRepositry userRepositry;

	public Users getUserByEmail(String emial) throws Exception {

		return this.userRepositry.findById(emial).orElseThrow(() -> new Exception("User id dosen't exist"));

		//return this.userRepositry.findById(email).orElseThrow(() -> new Exception("User id dosen't exist"));
	}
	
	public boolean insertUser(Users user) {
		
		if (this.userRepositry.existsById(user.getEmail())) {
			return false;
		}		
		this.userRepositry.save(user);
		return true;
	}
	
	



}
