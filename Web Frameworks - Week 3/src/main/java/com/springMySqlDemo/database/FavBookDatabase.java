package com.springMySqlDemo.database;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springMySqlDemo.model.FavBooks;
import com.springMySqlDemo.repositry.FavBookRepositry;
@Service
public class FavBookDatabase {
	
	@Autowired
	private FavBookRepositry favBookRepositry;
	
	public List<FavBooks> getFavBooks(){
		return this.favBookRepositry.findAll();
	}
	
	public boolean insertFavBook(FavBooks books) {
		this.favBookRepositry.save(books);
		return true;
		
	}

}
