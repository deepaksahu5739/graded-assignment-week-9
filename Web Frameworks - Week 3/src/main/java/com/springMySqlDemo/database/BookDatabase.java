package com.springMySqlDemo.database;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springMySqlDemo.model.Book;
import com.springMySqlDemo.repositry.BookRepositry;


@Service
public class BookDatabase {
	
	@Autowired
	private BookRepositry bookRepositry;
	
	public List<Book> getBookData(){
		
	  return this.bookRepositry.findAll();
		
	}
	
	public boolean insertBook(Book book) {
		
		this.bookRepositry.save(book);	
		return true;
	}
	
	

}
