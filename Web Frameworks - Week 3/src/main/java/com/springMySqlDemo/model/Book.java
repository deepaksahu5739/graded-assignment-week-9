package com.springMySqlDemo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = " bookdata")
public class Book {
	
	@Id
	@Column(name= "title")
	private String BookName;
	@Column(name = "author")
	private String acthor;
	@Column(name = "publishyear")
	private String publishYear;
	private String poster;
	
	public Book() {
		
	}

	public Book(String bookName, String acthor, String publishYear, String poster) {
		super();
		BookName = bookName;
		this.acthor = acthor;
		this.publishYear = publishYear;
		this.poster = poster;
	}

	public String getBookName() {
		return BookName;
	}

	public void setBookName(String bookName) {
		BookName = bookName;
	}

	public String getActhor() {
		return acthor;
	}

	public void setActhor(String acthor) {
		this.acthor = acthor;
	}

	public String getPublishYear() {
		return publishYear;
	}

	public void setPublishYear(String publishYear) {
		this.publishYear = publishYear;
	}

	public String getPoster() {
		return poster;
	}

	public void setPoster(String poster) {
		this.poster = poster;
	}

	@Override
	public String toString() {
		return "Book [BookName=" + BookName + ", acthor=" + acthor + ", publishYear=" + publishYear + ", poster="
				+ poster + "]";
	}
	
	

}
