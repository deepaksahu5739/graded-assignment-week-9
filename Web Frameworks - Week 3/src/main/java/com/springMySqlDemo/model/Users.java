package com.springMySqlDemo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
//@Table(name = "usersdata")
public class Users {
	
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	//@Column(name = "FirstName")
	private String FirstName;
	//@Column(name = "LastName")
	private String LastName;
	//@Column(name = "Phone")
	private String phone;

	@Id
	//@Column(name = "Email")
	private String email;
	//@Column(name = "Password")
	private String password;
	
	public Users() {
		
	}

	public Users(int id, String firstName, String lastName, String phone, String email, String password) {
		super();
		this.id = id;
		FirstName = firstName;
		LastName = lastName;
		this.phone = phone;
		this.email = email;
		this.password = password;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return FirstName;
	}

	public void setFirstName(String firstName) {
		FirstName = firstName;
	}

	public String getLastName() {
		return LastName;
	}

	public void setLastName(String lastName) {
		LastName = lastName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "Users [id=" + id + ", FirstName=" + FirstName + ", LastName=" + LastName + ", phone=" + phone
				+ ", email=" + email + ", password=" + password + "]";
	}
	
	

	
}
