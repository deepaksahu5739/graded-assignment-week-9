package com.springMySqlDemo.repositry;

import org.springframework.data.jpa.repository.JpaRepository;

import com.springMySqlDemo.model.FavBooks;

public interface FavBookRepositry extends JpaRepository<FavBooks, String> {
	

}
