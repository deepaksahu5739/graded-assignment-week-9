package com.springMySqlDemo.repositry;

import org.springframework.data.repository.CrudRepository;

import com.springMySqlDemo.model.Users;



public interface UserRepositry extends CrudRepository<Users, String> {

}
