package com.springMySqlDemo.repositry;

import org.springframework.data.jpa.repository.JpaRepository;

import com.springMySqlDemo.model.Book;

public interface BookRepositry extends JpaRepository<Book, String>{

}
