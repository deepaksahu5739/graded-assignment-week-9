<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@page import="java.util.*"%>


<%@page import="com.springMySqlDemo.model.FavBooks" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>

	
	<header>
		<a href="a" class="logo"><i>Ebooks</i></a>

		<nav class="navbar">
			<a href="index">Home</a>
			<a href='favbooks'>fav books</a>
			<a href="logout">Log Out</a>
			

		</nav>
	</header>
	
        

<%
	String email = (String)session.getAttribute("email");
	out.println("<h1> Hello " + email+ "</h1>");
	%>
	
	<%FavBooks movieName = (FavBooks)session.getAttribute("movieName");
	
	
	%>
	
	

<section class="home" id="home" style="padding: 10rem;">
        <div class="content">
            <h3> <%=movieName.getBookName() %> </h3>
            
            <p><%=movieName.getActhor() %> </p>
            <h3>Added to Fav List</h3>
           

        </div>
        <div class="image">
            <img src=<%=movieName.getPoster() %> alt="">

        </div>

    </section>


</body>

<style>
@charset "ISO-8859-1";

@import url('https://fonts.googleapis.com/css2?family=Nunito:ital,wght@0,200;0,300;0,400;0,700;1,600&display=swap');

:root{
    --red:#ff3838;
}

*{
    font-family: 'Nunito', sans-serif;
    margin: 0; padding: 0;
    box-sizing: border-box;
    outline: none; border: none;
    text-decoration: none;
    text-transform:capitalize;
    transition: all .2s linear;
    
}

*::selection{
    background:var(--red);
    color: #fff;
}

html{
    font-size: 62.5%;
    overflow-x: hidden;
    scroll-behavior: smooth;
    scroll-padding-top: 6rem;
}

body{
    background: #f7f7f7;
    
}

section{
    padding: 2rem  7rem 1%;
    

}
section .movielist{
    padding: 0;
    margin: 0;
}

header{
    position: fixed;
    top:0; left: 0; right: 0;
    z-index: 1000;
    display: flex;
    align-items: center;
    justify-content: space-between;
    background: #fff;
    padding: 2rem 9%;
    box-shadow: 0 .5rem 1rem rgba(0,0,0,.1);

}

header .logo{
    font-size: 2.5rem;
    font-weight: bolder;
    color: #666;
}

header .logo i{
    padding-right: .5rem;
    color: var(--red);
}

header .navbar a{
    font-size: 2rem; 
    margin-left: 2rem;
    color: var(--red);
}
header .navbar button{
    font-size: 2rem; 
    margin-left: 2rem;
    color: #666;
    background-color: #fff;

}

header .navbar a:hover{
    color: #666;
}

header ,.navbar button:hover{
    color: var(--red);
}

.home{
    display: flex;
    flex-wrap: wrap;
    gap: 1.5rem;
    min-height: 100vh;
    align-items: center;
    
    background-size: cover;
    background-position: center;
    
}

.home .content{
    flex: 1 1 40rem;
    padding-top: 6.5rem;
}

.home .image{
    flex: 0 1 52rem;
}

.home .image img{
    width: 100%;
    padding: 1rem;
    animation: float 3s linear infinite;
}

.home .content h3{
    font-size: 5rem;
    color:var(--red);
    transition: var(--red) 2s linear;

}

.home .content p{
    font-size: 1.7rem;
    color: #333;
    padding: 1rem 0;

}

</style>
</html>
