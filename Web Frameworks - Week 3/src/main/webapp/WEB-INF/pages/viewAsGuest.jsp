<%@ 
page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="java.util.*"%>


<%@page import="com.springMySqlDemo.model.Book" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css" href="style2.css">
</style>
</head>
<body>

	
	 <header>
        <a href="a" class="logo"><i>Ebooks</i></a>

        <nav class="navbar">
            <a href="index">Home</a>
            
            
            
        </nav>
    </header>
    
	
	<% 
	String name = (String)session.getAttribute("username");
	%>
	<h1>Welcome user </h1>

	<%

		
		List<Book> books = (List<Book>)session.getAttribute("bookList");
	
	%>

	<section class="movielist" id="movielist" style="margin: -3rem;">
		<h1 class="heading">book list</h1>
		<div class="box-container">
			
				<% 
		for(Book book : books){
		
						 
		%>
				<div class="box">
					<div class="content">
						<img alt="" src="<%=book.getPoster() %>"> <input type="text"
							name="author">
						<h3><%=book.getBookName() %></h3>
						<h3><%= book.getActhor() %></h3>

						<p>
							<%=book.getPublishYear() %>
						<p>

						<form action="welcome" method="post">
							<input type="hidden" id="thisField" name="bookPoster" value="<%=book.getPoster() %>">
							<input type="hidden" id="thisField" name="inputName" value="<%=book.getBookName() %>">
							<input type="hidden" id="thisField" name="bookAuthor" value="<%= book.getActhor() %>">
							<input type="hidden" id="thisField" name="bookyear" value="<%=book.getPublishYear()  %>">
							
							
							
							
							
							 </form>
					</div>

				</div>

				<%					
	
		}	
				
				
%>
		
		</div>
	</section>

</body>

<style>
@charset "ISO-8859-1";

    @import url('https://fonts.googleapis.com/css2?family=Nunito:ital,wght@0,200;0,300;0,400;0,700;1,600&display=swap');

    :root{
        --red:#ff3838;
    }

    *{
        font-family: 'Nunito', sans-serif;
        margin: 0; padding: 0;
        box-sizing: border-box;
        outline: none; border: none;
        text-decoration: none;
        text-transform:capitalize;
        transition: all .2s linear;
        
    }

    *::selection{
        background:var(--red);
        color: #fff;
    }

    html{
        font-size: 62.5%;
        overflow-x: hidden;
        scroll-behavior: smooth;
        scroll-padding-top: 6rem;
    }

    body{
        background: #f7f7f7;
    }

    section{
        padding: 11rem  7rem 1%;
        

    }
    section .movielist{
        padding: 0;
        margin: 0;
    }

    header{
        position: fixed;
        top:0; left: 0; right: 0;
        z-index: 1000;
        display: flex;
        align-items: center;
        justify-content: space-between;
        background: #fff;
        padding: 2rem 9%;
        box-shadow: 0 .5rem 1rem rgba(0,0,0,.1);

    }

    header .logo{
        font-size: 2.5rem;
        font-weight: bolder;
        color: #666;
    }

    header .logo i{
        padding-right: .5rem;
        color: var(--red);
    }

    header .navbar a{
        font-size: 2rem; 
        margin-left: 2rem;
        color: var(--red);
    }
    header .navbar button{
        font-size: 2rem; 
        margin-left: 2rem;
        color: #666;
        background-color: #fff;

    }

    header .navbar a:hover{
        color: #666;
    }

    header ,.navbar button:hover{
        color: var(--red);
    }

    .home{
        display: flex;
        flex-wrap: wrap;
        gap: 1.5rem;
        min-height: 100vh;
        align-items: center;
        
        background-size: cover;
        background-position: center;
        
    }

    .home .content{
        flex: 1 1 40rem;
        padding-top: 6.5rem;
    }

    .home .image{
        flex: 0 1 31rem;
    }

    .home .image img{
        width: 100%;
        padding: 1rem;
        animation: float 3s linear infinite;
    }

    .home .content h3{
        font-size: 5rem;
        color:var(--red);
        transition: var(--red) 2s linear;

    }

    .home .content p{
        font-size: 1.7rem;
        color: #333;
        padding: 1rem 0;

    }

    .fav{
        display: inline-block;
        padding: .8rem 3rem;
        border: .2rem solid var(--red);
        color: var(--red);
        cursor: pointer;
        font-size: 1.7rem;
        border-radius: .5rem;
        position: relative;
        overflow: hidden;
        z-index: 0;
        margin-top: 1rem;
        margin-right: 1rem;

    }

    .fav::before{
        content: '';
        position: absolute;
        top: 0; right: 0;
        width: 0%;
        height: 100%;
        background: var(--red);
        
        z-index: -1;
    }

    .fav:hover::before{
        width: 100%;
        left: 0;
    }
    .fav:hover{
        color: #fff;
        background: var(--red);
        

    }



    .heading{
        text-align: center;
        font-size: 3.5rem;
        padding: 1rem;
        color:var(--red);
        margin: 2rem;
    }
    .movielist .box-container{
        display: flex;
        flex-wrap: wrap;
        gap:1.5rem;    
    }
    .movielist .box-container .box{
        flex: 1 1 30rem;
        position: relative;
        overflow: hidden;
        box-shadow: 0 .5rem 1rem rgba(0,0,0,.1);
        
        cursor: pointer;
        border-radius: .5rem;
        padding-top: -7px;

    }
    .movielist .box-container .box .content{
        text-align: center;
        background: #fff;
        padding: 2rem;
    }
    .movielist .box-container .box .content img{
        margin: .5rem 0;

    }
    .movielist .box-container .box .content h3{
        font-size: 2.5rem;
        color: #333;
    }
    .movielist .box-container .box .content p{
        font-size: 1.5rem;
        color: #666;
        padding: 1rem 0;
    }

    .movielist .box-container .box .content para:hover{

        background: transparent;
        display: var(--red);
        text-align: center;
        font-size: 1.5rem;

    }


    @media(max-width:991px){
        html{
            font-size: 55%;
        }

        header{
            padding: 2rem;
        }
        section{
            padding: 2rem;
        }
    }

    @media(max-width:768px){
        header .navbar{
            position: absolute;
            top:100%; left: 0; right: 0;
            background: #f7f7f7;
            border-top: .1rem solid rgba(0,0,0,.1);
            clip-path: polygon(0 0, 100% 0, 100% 0, 0 0);
        }

        header .navbar a{
            margin: 1.5rem;
            padding: 1.5rem;
            display: block;
            border: 2rem solid rgba(0,0,0,.1);
            border-left: .5rem solid var(--red);
            background: #fff;
        }
        header .navbar button{
            margin: 1.5rem;
            padding: 1.5rem;
            display: block;
            border: 2rem solid rgba(0,0,0,.1);
            border-left: .5rem solid var(--red);
            background: #fff;
            background-color: #fff;

        }
    }

    @media(max-width:991px){
        html{
            font-size: 50%;
        }
    }

</style>
</html>